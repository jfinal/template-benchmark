package com.mitchellbosecke.benchmark;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.io.NoLockStringWriter;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import java.io.IOException;
import java.util.Map;

public class Beetl extends BaseBenchmark {

	GroupTemplate gt;
	Map<String, Object> data;

	@Setup
	public void setup() throws IOException {
		ClasspathResourceLoader resourceLoader = new MyClasspathResourceLoader("/");
		Configuration cfg = Configuration.defaultConfiguration();
		cfg.setStatementStart("@");
		cfg.setStatementEnd(null);
		gt = new GroupTemplate(resourceLoader, cfg);
		data = getContext();
	}

	@Benchmark
	public String benchmark() throws IOException {
		// beetl 的 Template 居然不能重用，每次使用必须重新获取，否则有异常
		Template template = gt.getTemplate("/templates/stocks.beetl.html");
		template.binding(data);
		
		NoLockStringWriter writer = new NoLockStringWriter();
		template.renderTo(writer);
		return writer.toString();
	}

	static class MyClasspathResourceLoader extends ClasspathResourceLoader {

		public MyClasspathResourceLoader(String root) {
			super(root);
		}

		@Override
		public void init(GroupTemplate gt) {
			Map<String, String> resourceMap = gt.getConf().getResourceMap();

			if (this.charset == null) {
				this.charset = resourceMap.get("charset");
			}
			this.setAutoCheck(false);
		}
	}

}




