package com.mitchellbosecke.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.rythmengine.RythmEngine;
import org.rythmengine.template.ITemplate;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class Rythm extends BaseBenchmark {

    private Map<String, Object> context;

    private static final String TEMPLATE = "stocks.rythm.html";
    private RythmEngine engine;

    @Setup
    public void setup() throws IOException {
        Properties p = new Properties();
        p.put("log.enabled", false);
        p.put("feature.smart_escape.enabled", false);
        p.put("feature.transform.enabled", false);
        p.put("home.template.dir", "templates");
        engine = new RythmEngine(p);
        this.context = getContext();
    }

    @Benchmark
    public String benchmark() {
        ITemplate template = engine.getTemplate(TEMPLATE, context);
        return template.render();
    }

}
